# Coding standards

To maintain the team code conformity, help versioning and keep a good code readability, some code styles was adopted by this development team.

Those code standards are checked in the code review process and with automated tests before each deploy.

## PHP Standard Style

This is the style adopted to write PHP files. [Read More About](https://www.php-fig.org/psr/)

## JavaScript Standard Style

This is the style adopted to write javascript files both in client side and in Node.js. [Read More About](https://standardjs.com/)

## Code Guide

For HTML and CSS the Code Gide by @mdo was adopted. [Read More About](https://codeguide.co/)

## BEM (Block Element Modifier)

Additionally this project uses the BEM methodology to classify elements and help with CSS performance and maintenance. [Read More About](http://getbem.com/introduction/)